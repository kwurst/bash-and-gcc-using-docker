#!/bin/bash

# Build shell.
docker-compose -f shell/docker-compose.yml build shell

# Run shell.
docker-compose -f shell/docker-compose.yml run --rm shell
