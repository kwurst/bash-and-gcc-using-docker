# Linux Shell Environment with bash and gcc using Docker

## Install Docker Desktop

- Mac: https://docs.docker.com/docker-for-mac/install/
- Windows 10 Pro, Enterprise, and Education: https://docs.docker.com/docker-for-windows/install/
- Windows 10 Home: https://docs.docker.com/docker-for-windows/install-windows-home/

## Running Shell

1. Make sure `Docker Desktop` is running on your computer.
2. Start up the shell: From this top-level directory, in the terminal type `shell/run.sh` or double-click `shell/run.bat`
    - This will take a while the first time as it builds the image. It will be much faster after that.
3. You will be put into the shell with the prompt `bash-5.1#`

## Working in the Shell

You are working in a directory tree rooted at this top-level directory.

If you want to use editors or IDEs on your host machine, you can do so for editing. For compiling and running code, you should do that in the bash shell.

## Stopping Shell

1. In the bash shell, enter the command: `exit`

## Installing Additional Software into the Shell

If you want to install more software in the docker container, edit line 2 of `shell/Dockerfile` to include the package(s) you want to install.
